IDIR=.
CC=g++
CFLAGS=-I$(IDIR) `root-config --cflags --glibs` -g

# PYBIND=`python -m pybind11 --includes` -fPIC --shared

# PYBINDIR=nanocppfw
BINDIR=bin
ODIR=obj
SRCDIR=src
INCDIR=interface
LIBS=-lm

_OBJ = Util.o ModuleTTreeUnpacker.o PixelModule.o QCoreFactory.o QCore.o RD53StreamEncoder.o AuroraFormatter.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

# Set up output directories
$(shell   mkdir -p $(BINDIR))
$(shell   mkdir -p $(ODIR))

$(ODIR)/%.o: $(SRCDIR)/%.cc
	$(CC) -c -o $@ $< $(CFLAGS) -fPIC

.PHONY: clean default

default: run_link_occupancy run_stream_size_per_dtc generate_binary_output_per_chip

run_link_occupancy: $(OBJ)
	$(CC) src/link_occupancy.cc -o $(BINDIR)/link_occupancy $^ $(CFLAGS) $(LIBS)

run_stream_size_per_dtc: $(OBJ)
	$(CC) src/run_stream_size_per_dtc.cc -o $(BINDIR)/run_stream_size_per_dtc $^ $(CFLAGS) $(LIBS)

generate_binary_output_per_chip: $(OBJ)
	$(CC) src/generate_binary_output_per_chip.cc -o $(BINDIR)/generate_binary_output_per_chip $^ $(CFLAGS) $(LIBS)

run_format_conversion: $(OBJ)
	$(CC) src/run_format_conversion.cc -o $(BINDIR)/run_format_conversion $^ $(CFLAGS) $(LIBS)

clean:
	rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~ $(BINDIR)/*

