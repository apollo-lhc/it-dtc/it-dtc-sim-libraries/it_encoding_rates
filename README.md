# it_encoding_rates

## Description

Code used to simulate RD53 event streams and faciliate downstream processing. 

## Setup
The only strict dependency is a recent version of ROOT and present-day C++. This package does not depend on CMSSW.
To compile, simply call `make`.

## Input
The processing is performed based on a flat input ROOT file containing digitized IT hits for the full detector. The input files are created with [this code](https://gitlab.cern.ch/aalbert/itdigiexporter/tree/ROOTIO).

An example file can be found on the lxplus EOS:
```
/eos/user/a/aalbert/itdigi/itdigi_TTBar14TeV_10_6_0_patch2_D41_PU200.root
```

## Usage
The processing is done in steps. There is no single `master` script, as the exact needs depend on the goal of a specific study. Roughly, the relevant classes are:
- `ModuleTTreeUnpacker`: Reads the input file and creates `PixelModule` instances. A `PixelModule` represents one single module of the IT, which can contain two or four readout chips.
- `QCoreFactory`: Creates `QCore` instances from `PixelModule`s.
- `RD53StreamEncoder`: Converts a list of `QCore` instances into a variable-length binary stream, as done in RD53.
- `AuroraFormatter`:  Applies AURORA-like block formatting to a given binary stream and adds new stream and chip ID bits where appropriate.

The `src/run_format_conversion.cc` script showcases how these classes are strung together. Check it out. It can be used as follows:

```
make run_format_conversion
./bin/run_format_conversion --file_in /eos/user/a/aalbert/itdigi/itdigi_TTBar14TeV_10_6_0_patch2_D41_PU200.root
 --module 1
```

The `module` commandline argument allows to pick one specific module for processing. The script will create an output ROOT file containing a TTree with per-event information about stream sizes for the chosen module. If desired, it is easy to modify the script in order to run over multiple modules, or a broader choise of modules, or whatever is needed.