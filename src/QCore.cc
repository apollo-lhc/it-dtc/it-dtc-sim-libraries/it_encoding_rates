#include <interface/QCore.h>
#include<interface/Util.h>
#include<iostream>
QCore::QCore(
             uint32_t ccol_in,
             uint32_t qcrow_in,
             bool isneighbour_in,
             bool islast_in,
             vector<uint32_t> adcs_in,
             vector<bool> hits_in
             ) :
    adcs(adcs_in),
    hits(hits_in),
    isneighbour(isneighbour_in),
    islast(islast_in),
    qcrow(qcrow_in),
    ccol(ccol_in)
    {
    };

vector<bool> QCore::get_row(int row_index) const {
    if((row_index < 0) or (row_index > 1)){
        throw;
    }
    vector<bool> row;
    row.insert(row.end(), this->hitmap.begin() + 8 * row_index, this->hitmap.begin() + 8 * (row_index+1));
    assert(row.size() == 8);
    return row;
}
