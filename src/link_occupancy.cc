#include<TFile.h>

#include<include/cxxopts.hpp>

#include<interface/ModuleTTreeUnpacker.h>
#include<interface/QCoreFactory.h>
#include<interface/RD53StreamEncoder.h>
#include<interface/AuroraFormatter.h>
#include<fstream>
#include<iostream>

using namespace std;

cxxopts::ParseResult cli(int argc, char *argv[]){
    cxxopts::Options options("ITRate", "One line description of MyProgram");
    options.add_options()
    ("file_in", "File to process",cxxopts::value<string>())
    ;
    auto opts = options.parse(argc, argv);
    return opts;
};

int const MAX_ENTRY=1000;
struct output_container {
    Int_t event;
    Int_t ndigis_on_det;
    Int_t nchip;

    // Int_t px[MAX_ENTRY];
    // Int_t py[MAX_ENTRY];
    // Int_t pcharge[MAX_ENTRY];
    Int_t raw_hits[4];
    Int_t stream_size_chip_raw[4];
    Int_t stream_size_chip_aurora[4];
    Int_t stream_size_chip_aurora_pad[4];
    Bool_t barrel;
    Int_t disk;
    Int_t layer;
    Int_t dtc;
    Int_t module_id;
    Int_t module_index;
    Int_t detid;

    void reset(){
        event = 0;
        ndigis_on_det = 0;
        for(int i=0; i < 4; i++){
            stream_size_chip_raw[i] = 0;
            stream_size_chip_aurora[i] = 0;
            stream_size_chip_aurora_pad[i] = 0;
        }
        barrel = false;
        disk = 0;
        layer = 0;
        dtc = 0;
        module_id = 0;
        module_index = 0;
    }
};

int main(int argc, char *argv[]){
    //auto opts = cli(argc, argv);
    string treepath("BRIL_IT_Analysis/Digis");
    string input_file_path = "/afs/cern.ch/user/s/syuan/cernbox/crabjobs/ITdigiExporter/RelValFourMuExtendedPt1_200_D88_V1/RelValFourMuExtendedPt1_200/ITdigiExporter/220420_181431/0000/RelValFourMuExtendedPt1_200_D88_V1.root";
    if (argc>1) input_file_path = argv[1];
    string output_file_path = "output/RelValFourMuExtendedPt1_200_D88_V1/link_occupancy.root";
    if (argc>2) output_file_path = argv[2];

    // Input
    ModuleTTreeUnpacker unpacker(input_file_path.c_str(), treepath);

    // Output
    output_container oc;
    
    TFile * outfile = new TFile(output_file_path.c_str(),"RECREATE");
    TTree * tree = new TTree();
    tree->Branch("event",                  &oc.event,                  "event/I");
    tree->Branch("nchip",                  &oc.nchip,                  "nchip/I");

    tree->Branch("raw_hits",        &oc.raw_hits,        "raw_hits[nchip]/I");
    tree->Branch("stream_size_chip_raw",        &oc.stream_size_chip_raw,        "stream_size_chip_raw[nchip]/I");
    tree->Branch("stream_size_chip_aurora",     &oc.stream_size_chip_aurora,     "stream_size_chip_aurora[nchip]/I");
    tree->Branch("stream_size_chip_aurora_pad", &oc.stream_size_chip_aurora_pad, "stream_size_chip_aurora_pad[nchip]/I");
    

    // tree->Branch("px",              oc.px,             "px[ndigis_on_det]/I");
    // tree->Branch("py",              oc.py,             "py[ndigis_on_det]/I");
    // tree->Branch("pcharge",         oc.pcharge,        "pcharge[ndigis_on_det]/I");
    tree->Branch("barrel",  &oc.barrel, "barrel/O");
    tree->Branch("disk",    &oc.disk,   "disk/I");
    tree->Branch("layer",   &oc.layer,  "layer/I");
    tree->Branch("dtc",     &oc.dtc,    "dtc/I");
    tree->Branch("module_id",  &oc.module_id, "module_id/I");
    tree->Branch("detid",  &oc.detid, "detid/I");
    tree->Branch("module_index",  &oc.module_index, "module_index/I");
    tree->SetDirectory(outfile);
    // Processing for stream size
    QCoreFactory qfactory;
    RD53StreamEncoder encoder;
    AuroraFormatter aurora;
    int nevents = 0;
    while(unpacker.next_event()) {
        oc.reset();
        oc.event = nevents;
        nevents++;
        int event = unpacker.get_event();

        for(int module_index=0; module_index<unpacker.get_nmodule(); module_index++) {

            auto pm = unpacker.get_module(module_index);

            oc.barrel = pm.barrel;
            oc.disk = pm.disk;
            oc.layer = pm.layer;
            oc.dtc = pm.dtc;
            oc.module_id = pm.module_id;
            oc.detid = pm.detid;
            oc.module_index = module_index;

            auto qcores_per_chip = qfactory.from_pixel_module_perchip(pm);
            oc.nchip = qcores_per_chip.size();
            auto raw_hits_per_chip = pm.count_hits_per_chip();

            for(int ichip = 0; ichip<qcores_per_chip.size(); ichip++) {

                oc.raw_hits[ichip] = raw_hits_per_chip[ichip];

                encoder.reset();
                encoder.serialize_event(qcores_per_chip.at(ichip), 1);

                auto stream_chip_raw = encoder.get_stream();
                oc.stream_size_chip_raw[ichip] = stream_chip_raw.size();

                auto stream_chip_aurora = aurora.apply_blocking(stream_chip_raw, 1);
                oc.stream_size_chip_aurora[ichip] = stream_chip_aurora.size();

                auto stream_chip_aurora_pad = aurora.orphan_pad(stream_chip_aurora);
                oc.stream_size_chip_aurora_pad[ichip] = stream_chip_aurora_pad.size();

            }

            tree->Fill();
        }
        //if(nevents>500) {
        //    break;
        //}
    }

    tree->Write("t");
    outfile->Close();
}
