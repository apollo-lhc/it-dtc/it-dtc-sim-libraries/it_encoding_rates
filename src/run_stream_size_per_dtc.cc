#include <iostream>
#include<TFile.h>
#include<TTreeReader.h>
#include<TTreeReaderArray.h>
#include<include/cxxopts.hpp>
#include<interface/Util.h>
#include<interface/ModuleTTreeUnpacker.h>
#include<interface/QCoreFactory.h>
#include<interface/RD53StreamEncoder.h>
#include <chrono>
#include<include/cxxopts.hpp>

using namespace std;

cxxopts::ParseResult cli(int argc, char *argv[]){
    cxxopts::Options options("ITRate", "One line description of MyProgram");
    options.add_options()
    ("file_in", "File to process",cxxopts::value<string>())
    ("start", "Event index to start on",cxxopts::value<int>())
    ("stop", "Event index to stop on",cxxopts::value<int>())
    ("file_out", "Output file path",cxxopts::value<string>())
    ;
    auto opts = options.parse(argc, argv);
    return opts;
};
int main(int argc, char *argv[]){

    auto opts = cli(argc, argv);
    string filepath("/home/albert/cernbox/itdigi/itdigi_TTBar14TeV_10_6_0_patch2_D41_PU200.root_TEST");
    string treepath("BRIL_IT_Analysis/Digis");


    ModuleTTreeUnpacker unpacker(opts["file_in"].as<string>().c_str(), treepath);
    QCoreFactory qfactory;
    RD53StreamEncoder encoder;

    TFile * outfile = new TFile(opts["file_out"].as<string>().c_str(),"RECREATE");
    TTree * outtree = new TTree();


    int ttree_event = 0;
    int ttree_ndtc = 0;
    vector<int> ttree_dtc;
    vector<int> ttree_stream_sizes;


    outtree->Branch("event",  &ttree_event);
    outtree->Branch("ndtc",  &ttree_ndtc);
    outtree->Branch("dtc",  &ttree_dtc);
    outtree->Branch("stream_size",  &ttree_stream_sizes);
    int start = opts["start"].as<int>();
    int stop = opts["stop"].as<int>();
    int nevents = -1;
    while(unpacker.next_event()) {
        nevents++;
        if(nevents < start or nevents > stop) {
            continue;
        }
        int event = unpacker.get_event();
        auto start = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        cout << std::ctime(&start) << " Event " << event << endl;

        std::map<int, int> dtc_to_size_map;
        for(int imod=0; imod<unpacker.get_nmodule(); imod++){
            auto pm = unpacker.get_module(imod);
            auto qcores = qfactory.from_pixel_module(pm);
            encoder.reset();
            encoder.serialize_event(qcores, 1);
            int stream_size = encoder.get_stream().size();
            dtc_to_size_map[pm.dtc]+=stream_size;
        }

        // Unroll map for writing to TTree
        ttree_event = event;
        ttree_dtc.clear();
        ttree_stream_sizes.clear();
        for(auto const & pair : dtc_to_size_map) {
            cout << pair.first << ":" << pair.second << endl;
            ttree_dtc.push_back(pair.first);
            ttree_stream_sizes.push_back(pair.second);
            ttree_ndtc++;
        }
        outtree->Fill();
    }
    outtree->Write("perdtc");
    outfile->Close();
}
