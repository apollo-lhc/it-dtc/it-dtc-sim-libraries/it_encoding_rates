#include <interface/ModuleTTreeUnpacker.h>
#include <interface/Util.h>
#include <interface/PixelModule.h>
#include <TString.h>

ModuleTTreeUnpacker::ModuleTTreeUnpacker(string filepath, string treepath) {
    TFile * file = new TFile(filepath.c_str());
    this->_reader.SetTree((TTree*)file->Get(treepath.c_str()));

    // Per-event quantities
    this->branch_event = new TTreeReaderValue<int> (_reader, "event");
    this->branch_nmodule = new TTreeReaderValue<int> (_reader, "nmodule");
    
    // Per-module quantities
    this->branch_module = new TTreeReaderArray<uint32_t>(_reader, "module");
    this->branch_detid  = new TTreeReaderArray<uint32_t>(_reader, "detid");
    this->branch_disk   = new TTreeReaderArray<uint32_t>(_reader, "disk");
    this->branch_layer  = new TTreeReaderArray<uint32_t>(_reader, "layer");
    this->branch_barrel = new TTreeReaderArray<bool>(_reader, "barrel");
    this->branch_elink  = new TTreeReaderArray<uint8_t>(_reader, "elink");
    this->branch_lpgbt  = new TTreeReaderArray<uint8_t>(_reader, "lpgbt");
    this->branch_dtc    = new TTreeReaderArray<uint16_t>(_reader, "dtc");

    // Per-pixel quantities
    this->branch_column    = new TTreeReaderArray<vector<unsigned int>>(_reader, "column");
    this->branch_row    = new TTreeReaderArray<vector<unsigned int>>(_reader, "row");
    this->branch_adc    = new TTreeReaderArray<vector<unsigned int>>(_reader, "adc");
    this->branch_hit    = new TTreeReaderArray<vector<unsigned int>>(_reader, "hit");
}

bool ModuleTTreeUnpacker::next_event(){
    return this->_reader.Next();
}

uint32_t ModuleTTreeUnpacker::get_nmodule(){
    return **branch_nmodule;
}
uint32_t ModuleTTreeUnpacker::get_event(){
    return **branch_event;
}

int64_t ModuleTTreeUnpacker::get_Nevents(){
    return this->_reader.GetEntries();
}


PixelModule ModuleTTreeUnpacker::get_module(int index){
    assert(index<this->get_nmodule());
    return PixelModule(
            (*branch_module).At(index),
            (*branch_detid).At(index),
            (*branch_disk).At(index),
            (*branch_layer).At(index),
            (*branch_barrel).At(index),
            (*branch_elink).At(index),
            (*branch_lpgbt).At(index),
            (*branch_dtc).At(index),
            (*branch_row).At(index),
            (*branch_column).At(index),
            (*branch_adc).At(index),
	    (*branch_hit).At(index)
        );
}
