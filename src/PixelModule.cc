#include <interface/PixelModule.h>
#include <iostream>

PixelModule::PixelModule(
                    uint32_t imodule_id,
                    uint32_t idetid,
                    uint32_t idisk,
                    uint32_t ilayer,
                    bool ibarrel,
                    uint8_t ielink,
                    uint8_t ilpgbt,
                    uint16_t idtc,
                    vector<uint32_t> row_in,
                    vector<uint32_t> column_in,
                    vector<uint32_t> adc_in,
		    vector<uint32_t> hit_in
                    ) :
                    module_id(imodule_id),
                    detid(idetid),
                    disk(idisk),
                    layer(ilayer),
                    barrel(ibarrel),
                    elink(ielink),
                    lpgbt(ilpgbt),
                    dtc(idtc),
                    row(row_in),
                    column(column_in),
                    adc(adc_in),
		    hit(hit_in)
                    {
                        // The pixel dimension depend on the number of chips on module
                        // Hardcoded for now since there is no way to read out the maximum dimension from the sparse matrix
                        if((this->layer > 2) || (!(this->barrel) && this->disk>8)) {
                            nchips = 4;
                            nrows = 1354;
                            ncols = 434;
                        }
                        else {
                            nchips = 2;
                            nrows = 672;
                            ncols = 434;
                        }
                    }


//A module contains either 4 chips or 2 chips
int PixelModule::get_nchips(){
    return nchips;
}

int PixelModule::count_hits(){
    return hit.size();
}

std::vector<int> PixelModule::count_hits_per_chip() {
    int nhits = row.size();
    std::vector<int> hits_per_chip(nchips, 0);
    int nrows = nchips/2;
    for (int ihit = 0; ihit < nhits; ihit++){
        int chip_row, chip_col;
        if (row[ihit]<=672) chip_row = 0;
        else if (row[ihit]>682) chip_row = 1;
        else continue;
        if (column[ihit] <= 216) chip_col = 0;
        else if (column[ihit] > 218) chip_col = 1;
        else continue;
        int ichip = chip_row + nrows*chip_col;
        hits_per_chip[ichip] ++;
    }
    return hits_per_chip;
}
