#include <numeric>
#include<iostream>
#include<interface/RD53StreamEncoder.h>
#include<assert.h>
#include<interface/LUT.h>
RD53StreamEncoder::RD53StreamEncoder(){
    this->stream.reserve(128*64);
};
vector<bool> RD53StreamEncoder::get_stream() {
    return stream;
}
void RD53StreamEncoder::reset(){
    stream.clear();
}
void RD53StreamEncoder::set_compression(bool compression) {
    this->do_compression = compression;
}
void RD53StreamEncoder::write_bits(vector<bool> bits_to_write) {
    stream.insert(
                  stream.end(),
                  bits_to_write.begin(),
                  bits_to_write.end());
};
void RD53StreamEncoder::write_bits(bool bit_to_write) {
    stream.push_back(bit_to_write);
};


vector<bool> RD53StreamEncoder::serialize_qcore_hitmap(vector<bool> hitmap) {
    assert(hitmap.size()==16);

    // Separate rows
    vector<bool> row1, row2;
    row1.insert(row1.end(), hitmap.begin(), hitmap.begin()+8);
    row2.insert(row2.end(), hitmap.begin()+8, hitmap.end());
    assert(row1.size()==8 and row2.size()==8);

    // Row OR information
    std::vector<bool> row_or = {0, 0};
    for( auto const bit : row1) {
        if(bit) {
            row_or[0] = 1;
            break;
        }
    }
    for( auto const bit : row2) {
        if(bit) {
            row_or[1] = 1;
            break;
        }
    }

    // Compress the components
    vector<bool> row_or_enc = enc2(row_or);
    vector<bool> row1_enc = enc8(row1);
    vector<bool> row2_enc = enc8(row2);

    vector<bool> encoded_hitmap;
    encoded_hitmap.reserve(row_or_enc.size() + row1_enc.size() + row2_enc.size());
    encoded_hitmap.insert(encoded_hitmap.end(), row_or_enc.begin(), row_or_enc.end());
    encoded_hitmap.insert(encoded_hitmap.end(), row1_enc.begin(), row1_enc.end());
    encoded_hitmap.insert(encoded_hitmap.end(), row2_enc.begin(), row2_enc.end());

    return encoded_hitmap;
};

void RD53StreamEncoder::add_orphan_padding() {
    /* 
    Zero-padding of current stream to multiple of 64 bits.
    */
    int bits_written = stream.size();
    int n_orphan = 63 - bits_written % 63;
    vector<bool> orphan_bits(n_orphan, false);
    write_bits(orphan_bits);
}
vector<bool> RD53StreamEncoder::serialize_qcore_tots(vector<uint32_t> adcs, vector <bool> hitmap) {

    assert(hitmap.size()==16);
    assert(adcs.size()==16);

    vector<bool> serialized_tots;

    for(int i=0; i<hitmap.size(); i++){
        if (hitmap.at(i)){
            vector<bool> adc_binary = adc_to_binary(adcs.at(i));
            serialized_tots.insert(
                                    serialized_tots.end(),
                                    adc_binary.begin(),
                                    adc_binary.end()
                                    );
        }
    }

    return serialized_tots;

}

void RD53StreamEncoder::serialize_event(vector<QCore> & qcores, int event){

    bool is_new_ccol = true;
    int bits_written = 0;
    vector<bool> event_tag = int_to_binary(event, 8);
    write_bits(event_tag);

    int nqcore=0;
    for(auto q: qcores){

        // ccol address
        if(is_new_ccol){
            vector<bool> ccol_address = int_to_binary(53, 6);
            write_bits(ccol_address);
        }
        // islast / isneighbour
        write_bits(q.islast);
        write_bits(q.isneighbour);

        // qrow address only if not neighbour
        if(not q.isneighbour){
            vector<bool> qrow_address = int_to_binary(q.qcrow, 8);
            write_bits(qrow_address);
        }

        // Binary-tree encoded hitmap
        if (this->do_compression) {
            auto serialized_hitmap = serialize_qcore_hitmap(q.hits);
            write_bits(serialized_hitmap);
        }
        else {
            write_bits(q.hits);
        }

        // ToT values if wanted
        if(write_tot){
            auto serialized_tots = serialize_qcore_tots(q.adcs, q.hits);
            write_bits(serialized_tots);
        }

/*
        vector<bool> hitmap = q.hits;
        vector<uint32_t> totmap = q.adcs;
        std::cout << std::accumulate(hitmap.begin(), hitmap.end(), 0) << std::endl;
        std::cout<<"hitmap : ";
        for (int i=0; i<hitmap.size(); i++){
            std::cout<<hitmap.at(i)<<" ";
        }
        std::cout<<std::endl;

        std::cout<<"totmap : ";
        for (int i=0; i<totmap.size(); i++){
            std::cout<<totmap.at(i)<<" ";
        }
        std::cout<<std::endl;

        std::cout<<std::endl;*/
        // Next qcore needs to print ccol address
        // if this qcore is the last in its column
        is_new_ccol = q.islast;
    }

}
void RD53StreamEncoder::finalize(){
    this->add_orphan_padding();
}
