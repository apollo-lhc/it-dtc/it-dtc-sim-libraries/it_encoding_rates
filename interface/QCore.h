#ifndef QCORE_H
#define QCORE_H
#include<stdint.h>
#include<vector>

using namespace std;
class QCore{

    public:
        QCore(
              uint32_t ccol_in,
              uint32_t qcrow_in,
              bool isneighbour_in,
              bool islast_in,
              vector<uint32_t> adcs_in,
	      vector<bool> hits_in
              );
        vector<uint32_t> adcs;
	vector<bool> hits;
        vector<bool> hitmap;
        vector<bool> encoded_hitmap;
        bool islast;
        bool isneighbour;
        uint32_t ccol;
        uint32_t qcrow;
        int event;

        vector<bool> get_row(int row_index) const;
};
#endif // QCORE_H
