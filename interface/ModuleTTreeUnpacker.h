#ifndef MODULETTREEUNPACKER_H
#define MODULETTREEUNPACKER_H

#include <vector>
#include <stdint.h>
#include <assert.h>
#include<TTreeReader.h>
#include<TTreeReaderArray.h>
#include<TFile.h>
#include<interface/Util.h>
#include<interface/PixelModule.h>
using namespace std;

class ModuleTTreeUnpacker{
    public:
        ModuleTTreeUnpacker(string filepath, string treepath);

        bool next_event();
        uint32_t get_nmodule();
        uint32_t get_event();
        int64_t get_Nevents();

        PixelModule get_module(int index);

    private:
        TTreeReader _reader;
        TFile * _tfile;


        // Per-event quantities
        TTreeReaderValue<int> * branch_event;
        TTreeReaderValue<int> * branch_nmodule;

        // Per-module quantities
        TTreeReaderArray<uint32_t> * branch_module;
        TTreeReaderArray<uint32_t> * branch_detid;
        TTreeReaderArray<uint32_t> * branch_disk;
        TTreeReaderArray<uint32_t> * branch_layer;
        TTreeReaderArray<bool> * branch_barrel;
        TTreeReaderArray<uint8_t> * branch_elink;
        TTreeReaderArray<uint8_t> * branch_lpgbt;
        TTreeReaderArray<uint16_t> * branch_dtc;

        // Per-pixel quantities
        TTreeReaderArray<vector<uint32_t>> * branch_column;
        TTreeReaderArray<vector<uint32_t>> * branch_row;
        TTreeReaderArray<vector<uint32_t>> * branch_adc;
        TTreeReaderArray<vector<uint32_t>> * branch_hit;

};

#endif
