#ifndef PIXELMODULE_H
#define PIXELMODULE_H

#include <stdint.h>
#include <vector>
#include <interface/Util.h>
using namespace std;

class PixelModule{
    public:
        PixelModule(
                    uint32_t imodule_id,
                    uint32_t idetid,
                    uint32_t idisk,
                    uint32_t ilayer,
                    bool ibarrel,
                    uint8_t ielink,
                    uint8_t ilpgbt,
                    uint16_t idtc,
                    vector<uint32_t> row,
                    vector<uint32_t> column,
                    vector<uint32_t> adc,
		    vector<uint32_t> hit
                    );


        // Per-module quantities
        uint32_t nchips;
        uint32_t nrows;
        uint32_t ncols;
        uint32_t const module_id;
        uint32_t const detid;
        uint32_t const disk;
        uint32_t const layer;
        bool const barrel;
        uint32_t const elink;
        uint32_t const lpgbt;
        uint32_t const dtc;
        vector<uint32_t> row;
        vector<uint32_t> column;
        vector<uint32_t> adc;
        vector<uint32_t> hit;

        int get_nchips();

        int count_hits();

        std::vector<int> count_hits_per_chip();


};


#endif // PIXELMODULE_H
